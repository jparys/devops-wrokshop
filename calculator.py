def sum(a,b):
    return a + b

def subtraction(a,b) :
    return a - b

def multiplicaton (a,b):
    return a * b

def division(a,b):
    return a / b

operations = {
    '+' : sum,
    '-' : subtraction,
    'x' : multiplicaton,
    '/' : division
}

def process_string(input) : 
    inp = input.split()
    param1 = int(inp[1])
    param2 = int(inp[2])
    # operation = operations[inp[0]]
    # return operation(param1, param2) 
    return process_operation(inp[0], param1, param2)

def process_operation(op, p1, p2) : 
    #print(op, p1, p2)
    a = int(p1)
    b = int(p2)
    operation = operations[op]
    return operation(a, b) 


# user_instruction = input(" input > ")
# result = process_string(user_instruction)
# print(result) 