from calculator import process_string, process_operation
import math

f = open("step_3.txt", mode='r')

content = f.read()
lines = content.split('\n')

# result = []
# for item in lines :
#     elements = item.split()
#     result.append(process_operation(elements[1], elements[2], elements[3]))
# sum = 0
# for e in result:
#     sum += e
# sume is 756216.3057730488 

seen ={}

def process_instruction(input):
    itms = input.split()
    if len(itms) == 2 :
       
        # look up for next instracton
        new_line = itms[1]
        lookup_line(int(new_line))
    else :
        #
        new_line = process_operation(itms[2], itms[3], itms[4])
        new_line= math.floor(int(new_line))
        lookup_line(new_line)
        
last_line =[]

def lookup_line(nr) :
    if (nr-1) in seen :
        last_line.append(nr)
        last_line.append(seen[nr-1])
        return 
    line = lines[nr-1]
    seen[nr-1] = line
    process_instruction(line) 

lookup_line(1)

print(last_line)